#install snapd and reboot
apt install snapd -y && \
reboot

#after reboot
sudo snap install core; sudo snap refresh core && \
sudo snap install --classic certbot && \
sudo ln -s /snap/bin/certbot /usr/bin/certbot && \
sudo certbot --nginx

#test the SSL
sudo certbot renew --dry-run