# Instructions:

**Youtube instructional video:**
\
https://youtu.be/QiG6UbRM9co
\
\
Project Dependencies that you need to install:
\
Git, Docker, Docker-Compose
\
\
Clone the repository and cd into the project folder:
\
`git clone https://gitlab.com/tutorials47/web/microservices/flask-express-golang && cd flask-express-golang`
\
\
Start the microservices:
\
`docker-compose up`
\
\
Stop the microservices:
\
`docker-compose down`
