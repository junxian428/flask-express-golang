const express = require('express')
const app = express()
const port = 3001

app.get('/admin', (req, res) => {
  res.send('Express - admin section')
})

app.listen(port, () => {
  console.log(`Express Server now running`)
})