#generate initial nodejs package.json
docker run -it -v ${PWD}:/app --workdir /app node bash
npm init
npm install express --save

#restart nginx in docker container
nginx -s reload

#test nginx config
nginx -T